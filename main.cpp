#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <semaphore.h>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include "Helper.h"
#include <signal.h>

/**
 * Вариант 1 ACIL
 *  A – независимые процессы;
 *  C – мьютексы;
 *  I – каналы FIFO;
 *  L – квадратное уравнение:
 *  D=b^2-4ac,
 *  x1=(-b+sqrtPid(D))/2a
 *  x2=(-b-sqrtPid(D))/2a
 */

pid_t plusPid, mulPid, sqrtPid, divisionPid, mainPid;
const std::string nameMul = "mulPid";
const std::string nameSqrt = "sqrtPid";
const std::string namePlus = "plusPid";
const std::string nameDivision = "divisionPid";

pthread_mutex_t mulMutex, plusMutex, divisionMutex, sqrtMutex;
int mulPipe[2], plusPipe[2], divisionPipe[2], sqrtPipe[2];

int mutexPlusFile;

double GetOperation(double &a, double &b, pthread_mutex_t *mainMutex, const char *mainWay, pthread_mutex_t *addMutex);

double inputVariable(char charSymbol) {
    double digit;
    std::cout << "Input " << charSymbol << ": ";
    std::cin >> digit;
    return digit;
}

void startProcess(const std::string &name) {
    std::string way = "./";
    int id = execv(way.append(name).c_str(), {});
    if (id) {
        std::cout << "Process " << way << " was started." << std::endl;
    }
}

int main() {
    std::cout << "Hi. It's program for calculate ax^2+bx+c" << std::endl;
    double a = inputVariable('a'),
            b = inputVariable('b'),
            c = inputVariable('c');


    pthread_mutex_t *mainPlusMutex = init_mutex(CREATE, (char *) "main_plus_mutex");
    pthread_mutex_t *mainMulMutex = init_mutex(CREATE, (char *) "main_mul_mutex");
    pthread_mutex_t *mainDivMutex = init_mutex(CREATE, (char *) "main_div_mutex");
    pthread_mutex_t *mainSqrtMutex = init_mutex(CREATE, (char *) "main_sqrt_mutex");
    if (mainPlusMutex == nullptr) {
        std::cout << "Mutex wasn't initialized\n";
    }

    print("Mutex was initialed");
    const char *mainWay = "/tmp/main",
            *plusWay = "/tmp/plus";

    mkfifo(mainWay, 0666);
    mainPid = fork();
    if (mainPid) {
        // Ожидаем запуска и инициализации всех процессов.
        sleep(1);
        pthread_mutex_t *addMutex = init_mutex(GET, (char *) "add_mutex");
        pthread_mutex_t *mulMutex = init_mutex(GET, (char *) "mul_mutex");
        pthread_mutex_t *divMutex = init_mutex(GET, (char *) "division_mutex");
        pthread_mutex_t *sqrtMutex = init_mutex(GET, (char *) "sqrt_mutex");

        // ожидаем запуска процесса
        pthread_mutex_lock(addMutex);
        pthread_mutex_lock(mainPlusMutex);
        pthread_mutex_lock(mulMutex);
        pthread_mutex_lock(mainMulMutex);
        pthread_mutex_lock(divMutex);
        pthread_mutex_lock(mainDivMutex);
        pthread_mutex_lock(sqrtMutex);
        pthread_mutex_lock(mainSqrtMutex);

//        pthread_mutex_lock(divMutex);
        print("Main process was started");
        print("Plus process was started");

        // D=b^2-4ac;

        double powB2 = GetOperation(b, b, mainMulMutex, mainWay, mulMutex);
        double four = 4;
        double a4 = GetOperation(a, four, mainMulMutex, mainWay, mulMutex);
        double ac4 = GetOperation(a4, c, mainMulMutex, mainWay, mulMutex);
        ac4 = -ac4;
        double diskriminant = GetOperation(powB2, ac4, mainPlusMutex, mainWay, addMutex);
        double sqrtDiskriminant = GetOperation(diskriminant, ac4, mainSqrtMutex, mainWay, sqrtMutex);
        double minusB = -b;
        double two = 2;
        double a2 = GetOperation(a, two, mainMulMutex, mainWay, mulMutex);
        double x1 = GetOperation(minusB, sqrtDiskriminant, mainPlusMutex, mainWay, addMutex);
        x1 = GetOperation(x1, a2, mainDivMutex, mainWay, divMutex);

        double minusSqrtDiskriminant = -sqrtDiskriminant;
        double x2 = GetOperation(minusB, minusSqrtDiskriminant, mainPlusMutex, mainWay, addMutex);
        x2 = GetOperation(x2, a2, mainDivMutex, mainWay, divMutex);


        std::cout << "D = " << diskriminant << std::endl;
        std::cout << "sqrt( D ) = " << sqrtDiskriminant << std::endl;
        std::cout << "x1 = " << x1 << std::endl;
        std::cout << "x2 = " << x2 << std::endl;


/**
 * Примеры вызова

        GetOperation(a, b, mainPlusMutex, mainWay, addMutex);
        GetOperation(b, c, mainMulMutex, mainWay, mulMutex);
        GetOperation(b, c, mainDivMutex, mainWay, divMutex);
        GetOperation(a, c, mainSqrtMutex, mainWay, sqrtMutex);
*/

        kill(plusPid, SIGKILL);
        kill(sqrtPid, SIGKILL);
        kill(mulPid, SIGKILL);
        kill(divisionPid, SIGKILL);
        return 0;
    }

    plusPid = fork();
    if (plusPid) {
        startProcess("plus");

        return 0;
    }


    mulPid = fork();
    if (mulPid) {
        startProcess("mul");

        return 0;
    }

    divisionPid = fork();
    if (divisionPid) {
        startProcess("div");

        return 0;
    }

    sqrtPid = fork();
    if (sqrtPid) {
        startProcess("sqrt");

        return 0;
    }

    return 0;
}

double GetOperation(double &a, double &b, pthread_mutex_t *mainMutex, const char *mainWay, pthread_mutex_t *addMutex) {
    pthread_mutex_unlock(addMutex);
    int fifoBuffer = open(mainWay, O_WRONLY);
    pthread_mutex_lock(mainMutex);

    write(fifoBuffer, &a, sizeof(double));
    write(fifoBuffer, &b, sizeof(double));

    pthread_mutex_unlock(addMutex);
    close(fifoBuffer);
    pthread_mutex_lock(mainMutex);

    pthread_mutex_unlock(addMutex);
    fifoBuffer = open(mainWay, O_RDONLY);
    pthread_mutex_lock(mainMutex);

    double c;
    read(fifoBuffer, &c, sizeof(double));
    return c;
}
