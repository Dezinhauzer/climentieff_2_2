#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <semaphore.h>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include "Helper.h"

int main() {
    const char *mainWay = "/tmp/main",
            *plusWay = "/tmp/plus";
    mkfifo(mainWay, 0666);
    pthread_mutex_t *add_mutex = init_mutex(CREATE, (char *) "mul_mutex");
    if (add_mutex == nullptr) {
        std::cout << "Add mutex wasn't initialized\n";
    }

    pthread_mutex_lock(add_mutex);
    pthread_mutex_t *mainMutex = init_mutex(GET, (char *) "main_mul_mutex");
    if (mainMutex == nullptr) {
        std::cout << "Mutex wasn't initialized\n";
    }

    // Ожидаем подключение Мьютекса с главного процесса
    sleep(3);
    pthread_mutex_unlock(add_mutex);

    pthread_mutex_lock(mainMutex);
    print("start Mul");
    pthread_mutex_unlock(mainMutex);
    mkfifo(mainWay, 0666);

    while (1) {
        // Ожидаем вызова с главного потока
        pthread_mutex_lock(add_mutex);
        int fifoBuffer = open(mainWay, O_RDONLY);
        pthread_mutex_unlock(mainMutex);

        pthread_mutex_lock(add_mutex);
        double a, b;
        read(fifoBuffer, &a, sizeof(double));
        read(fifoBuffer, &b, sizeof(double));
        double c = a * b;
        std::cout << a << " * " << b << " = " << c << std::endl;
        close(fifoBuffer);

        pthread_mutex_unlock(mainMutex);
        pthread_mutex_lock(add_mutex);
        fifoBuffer = open(mainWay, O_WRONLY);

        write(fifoBuffer, &c, sizeof(double));
        pthread_mutex_unlock(mainMutex);
    }

    print("end plus");

    return 0;
}
