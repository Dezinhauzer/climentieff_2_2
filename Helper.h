#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <semaphore.h>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>

#ifndef UNTITLED_HELPER_H
#define UNTITLED_HELPER_H


class Helper {

};

enum mutex_action {
    CREATE, GET
};

pthread_mutex_t *init_mutex(mutex_action action, char *mutex_name);

void print(std::string str);


#endif //UNTITLED_HELPER_H
