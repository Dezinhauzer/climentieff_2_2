#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include "netinet/in.h"
#include "iostream"
#include "unistd.h"
#include "sys/socket.h"
#include <sys/types.h>

#include "Helper.h"

void print(std::string str) {
    std::cout << str << std::endl;
}

pthread_mutex_t *init_mutex(mutex_action action, char *mutex_name) {
    int shm_fd;
    if (action == CREATE) {
        shm_fd = shm_open(mutex_name, O_RDWR | O_CREAT, 0777);
    }

    if (action == GET) {
        shm_fd = shm_open(mutex_name, O_RDWR, 0777);
    }

    if (shm_fd == -1) {
        std::cout << mutex_name << "\tShared memory descriptor wasn't initialized\n";
        return {};
    }

    if (ftruncate(shm_fd, sizeof(pthread_mutex_t)) != 0) {
        std::cout << "File truncate was failed\n";
        return {};
    }

    void *addr = mmap(
            nullptr,
            sizeof(pthread_mutex_t),
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            shm_fd,
            0
    );
    if (addr == MAP_FAILED) {
        std::cout << "Shared address wasn't initialized\n";
        return {};
    }

    auto *mutex = (pthread_mutex_t *) addr;
    if (action == CREATE) {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
        pthread_mutex_init(mutex, &attr);
    }

    return mutex;
}
